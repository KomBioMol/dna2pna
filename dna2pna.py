import numpy as np
import sys

from top_edit import Top


class DNA:
    def __init__(self, filename, chain_to_mod=None):
        # TODO maybe write an interface so that reslist/residues all point to sections of self.content
        self.chain_to_mod = chain_to_mod
        self.chains = None
        self.filename = filename
        self.content = [line.strip() for line in open(self.filename) if line.startswith("ATOM")]
        self.anchor_atoms = ("O3'", "P", "C1'")
        self.reslist, self.residues = self.get_residues()
        self.alias_reslist = {r: r for r in self.reslist}
        self.atoms = [l[12:16].strip() for l in self.content]
        self.movable = None
        self.fixed = None
        self.heavy = None
        self.topo = None
        self.sp2 = None
        self.alt_anchors = None
        self.opt = Optimizer(self)
        self.bb = Backbone()
        self.process()
        
    def process(self):
        """
        performs the actual processing and saves the result
        :return: None
        """
        self.make_bb()
        self.optimize()
        self.write_bb()
        
    def optimize(self):
        """
        The routine that calls the Optimizer instance
        and performs the actual optimization, then swaps
        original content with optimized coords
        :return: None
        """
        self.update_topo()
        self.opt.update()
        self.opt.select_dna_atoms()
        self.opt.steep()
        for n, coords in enumerate(self.opt.coords):
            line = self.content[n]
            modline = "{:17s}{:8.3f}{:8.3f}{:8.3f}{}".format(
                line[:30],
                *coords,
                line[54:])
            self.content[n] = modline
        # self.reslist, self.residues = self.get_residues()
        # self.alias_reslist = {r: r for r in self.reslist}
        # for res in self.reslist:
        #     self.trans_peptidebond(res)
        # new_content = []
        # for res in self.reslist:
        #     for line in self.residues[res]:
        #         new_content.append(line.lstrip() + '\n')
        # self.content = new_content
    
    def update_topo(self):
        """
        Updates all topology info: atom numbers, subsets,
        bonds, residue starting atoms etc.
        :return: None
        """
        self.atoms = [l.split()[2] for l in self.content]
        bb_atoms = [l[12:16].strip() for l in self.bb.content]
        self.movable = [n for n, i in enumerate(self.atoms) if i in bb_atoms]
        self.fixed = [n for n, i in enumerate(self.atoms) if i not in bb_atoms]
        self.heavy = [n for n in range(len(self.atoms)) if not self.atoms[n].startswith('H')]
        bonds = self.bb.bonds
        sp2 = self.bb.sp2
        
        if self.chain_to_mod:
            inits = {self.chain_to_mod: [n for n, i in enumerate(self.atoms)
                     if i == bb_atoms[0] and self.content[n][21] == self.chain_to_mod]}
        else:
            inits = {ch: [n for n, i in enumerate(self.atoms)
                     if i == bb_atoms[0] and self.content[n][21] == ch] for ch in self.chains}
        base_anchors = [n for n, line in enumerate(self.content)
                        if (line.split()[3][:2] in ['XG', 'XA'] and line.split()[2] == "N9")
                        or (line.split()[3][:2] in ['XC', 'XT', 'XU'] and line.split()[2] == "N1")]
        self.topo = []
        for ch in inits.keys():
            self.topo += [(b[0]+i, b[1]+i) for i in inits[ch] for b in bonds]
            self.topo += [(i+4, j+9) for i, j in zip(inits[ch][:-1], inits[ch][1:])]
        all_inits = [i for k in inits.keys() for i in inits[k]]
        assert len(base_anchors) == len(all_inits)
        self.topo += [(i, j) for i, j in zip(all_inits, base_anchors)]
        self.sp2 = [a+i for a in sp2 for i in all_inits]
        self.topo += self.terminal_bonds()
    
    def write_bb(self):
        """
        saves the PDB file after processing
        :return: None
        """
        # TODO add charges to termini in .rtp
        # TODO optionally acetylate/methylate termini
        outfile = 'pna_' + self.filename
        with open(outfile, 'w') as o:
            for n, line in enumerate(self.content):
                o.write(self.renum(line, n+1).strip() + '\n')
    
    @staticmethod
    def renum(line, n):
        return line[:6] + "{:5d}".format(n) + line[11:]
    
    def make_bb(self):
        """
        performs the residue-wise alignment by first scaling the backbone
        to match DNA anchors' geometry, then translating to a common COG
        and finally rotating so that the anchors' positions are aligned properly;
        afterwards, DNA backbone atoms are removed and backbone atoms are
        inserted instead; then some extra magic happens with the naming,
        and termini are rebuilt
        :return: none
        """
        for res in self.reslist:
            self.bb.reset_coords()
            res_lines = self.residues[res]
            contain_anchors = all([any(anchor in line for line in res_lines) for anchor in self.anchor_atoms])
            if not contain_anchors:
                self.alt_anchors = ("O3'", "O5'", "C1'")
                print('alt anchors are being used for residue {} (probably lacking the phosphate atom)'.format(res))
            elif self.chain_to_mod and self.chain_to_mod != res.split('-')[2]:
                continue
            else:
                self.alt_anchors = None
            dna_anchor_positions = self.find_anchor_coordinates(res_lines)
            self.bb.scaleby(dna_anchor_positions)
            pna_anchor_positions = self.bb.anchor_coords
            trans_vec = np.mean(pna_anchor_positions, 0) - np.mean(dna_anchor_positions, 0)
            self.bb.moveby(trans_vec)
            rot_matr = self.opt_rotation_matrix(self.multiply_atoms(pna_anchor_positions -
                                                                    np.mean(pna_anchor_positions, 0)),
                                                self.multiply_atoms(dna_anchor_positions -
                                                                    np.mean(dna_anchor_positions, 0)))
            self.bb.rotby(rot_matr)
            self.exchange_backbone(res, self.bb)
        for res in self.reslist:
            self.trans_peptidebond(res)
        if self.chain_to_mod:
            self.add_termini(self.chain_to_mod)
        else:
            self.chains = {res.split('-')[2] for res in self.reslist}
            for ch in self.chains:
                self.add_termini(ch)
        for res in self.reslist:
            if self.chain_to_mod and self.chain_to_mod != res.split('-')[2]:
                pass
            else:
                self.residues[res] = self.set_name(self.alias_reslist[res], res)
        self.update_content()
        
    def update_content(self):
        """
        Matches the content of self.residues
        with that of self.content if both were
        modified in different ways (kind of redundant)
        :return:
        """
        new_content = []
        for res in self.reslist:
            for line in self.residues[res]:
                new_content.append(line.lstrip() + '\n')
        self.content = new_content
            
    def trans_peptidebond(self, res):
        """
        Ensures all backbone peptide bonds end up in a trans- conformation
        prior to optimization
        :param res: int, number of the currently processed residue
        :return: None
        """
        # TODO we need a post-opt check of cis-trans isomers
        res_index = self.reslist.index(res)
        if res_index == len(self.reslist)-1:
            return
        else:
            next_res = self.reslist[res_index + 1]
            if res.split('-')[2] != next_res.split('-')[2] or (self.chain_to_mod
                                                               and res.split('-')[2] != self.chain_to_mod):
                return
        res_content = self.residues[res]
        next_res_content = self.residues[next_res]
        c5p_xyz = self.extract_coords(self.find_line(res_content, "C5'"))
        cp_xyz = self.extract_coords(self.find_line(res_content, "C'"))
        n1p_xyz = self.extract_coords(self.find_line(next_res_content, "N1'"))
        v1 = cp_xyz - c5p_xyz
        v2 = cp_xyz - n1p_xyz
        v3 = 1.25 * (v1+v2)/np.linalg.norm(v1+v2)
        v4 = -1 * (v1+v2)/np.linalg.norm(v1+v2)
        o1p_xyz = cp_xyz + v3
        h1p_xyz = n1p_xyz + v4
        for linenum, line in enumerate(self.residues[res]):
            if line[12:16].strip() == "O1'":
                modline = "{:30s}{:8.3f}{:8.3f}{:8.3f}{}".format(
                    line[:30],
                    o1p_xyz[0],
                    o1p_xyz[1],
                    o1p_xyz[2],
                    line[54:])
                self.residues[res][linenum] = modline
        for linenum, line in enumerate(self.residues[next_res]):
            if line[12:16].strip() == "H1'1":
                modline = "{:30s}{:8.3f}{:8.3f}{:8.3f}{}".format(
                    line[:30],
                    h1p_xyz[0],
                    h1p_xyz[1],
                    h1p_xyz[2],
                    line[54:])
                self.residues[next_res][linenum] = modline
    
    def add_termini(self, modchain):
        # TODO rewrite to have modular termini database
        first_res = [res for res in self.reslist if res.split('-')[2] == modchain][0]
        last_res = [res for res in self.reslist if res.split('-')[2] == modchain][-1]
        self.add_nh3(first_res)
        self.add_co2(last_res)
    
    def add_nh3(self, res):
        # TODO this should really be method of the PNA class(?)
        res_content = self.residues[res]
        self.alias_reslist[res] = res.split('-')[0] + '5-' + '-'.join(res.split('-')[1:3])
        plane_atoms = ["C2'", "N1'", "H1'1"]
        plane_coords = [self.extract_coords(self.find_line(res_content, atom)) for atom in plane_atoms]
        perp = np.cross(plane_coords[1]-plane_coords[2], plane_coords[1]-plane_coords[0])
        new_coord1 = plane_coords[1] + 0.4 * (plane_coords[1] - plane_coords[2]) + 0.45 * (
                    plane_coords[1] - plane_coords[0]) + 0.6*perp
        new_coord2 = plane_coords[1] + 0.4 * (plane_coords[1] - plane_coords[2]) + 0.45 * (
                    plane_coords[1] - plane_coords[0]) - 0.6*perp
        # index = res_content.index(self.find_line(res_content, plane_atoms[2]))
        self.residues[res].append(self.write_line(self.find_line(res_content, "H1'1"), "H1'2", new_coord1))
        self.residues[res].append(self.write_line(self.find_line(res_content, "H1'1"), "H1'3", new_coord2))
        
    def add_co2(self, res):
        # TODO Ok I really need to figure out how modular I want it to be
        res_content = self.residues[res]
        self.alias_reslist[res] = res.split('-')[0] + '3-' + '-'.join(res.split('-')[1:3])
        plane_atoms = ["C5'", "C'", "O1'"]
        plane_coords = [self.extract_coords(self.find_line(res_content, atom)) for atom in plane_atoms]
        new_coord = plane_coords[1] + 0.7*(plane_coords[1]-plane_coords[2]) + 0.7*(plane_coords[1]-plane_coords[0])
        # index = res_content.index(self.find_line(res_content, plane_atoms[2]))
        self.residues[res].append(self.write_line(self.find_line(res_content, "O1'"), "O1'2", new_coord))

    def terminal_bonds(self):
        # TODO modularize by providing a list of terminal atoms in PNA class
        cterm_oxy = [n for n, line in enumerate(self.content) if line.split()[2] == "O1'2"]
        nterm_hydr = [n for n, line in enumerate(self.content) if line.split()[2] in ["H1'2", "H1'3"]]
        cterm_res = [line.split()[3] for line in self.content if line.split()[2] == "O1'2"]
        nterm_res = [line.split()[3] for line in self.content if line.split()[2] in ["H1'2", "H1'3"]]
        cterm_anchor = [n for n, line in enumerate(self.content) if line.split()[2] == "C'"
                        and line.split()[3] in cterm_res]
        nterm_anchor = [n for n, line in enumerate(self.content) if line.split()[2] == "N1'"
                        and line.split()[3] in nterm_res]
        t_bonds = []
        for c, o in zip(cterm_anchor, cterm_oxy):
            t_bonds.append((c, o))
        for n, h in zip(nterm_anchor, nterm_hydr[::2]):
            t_bonds.append((n, h))
        for n, h in zip(nterm_anchor, nterm_hydr[1::2]):
            t_bonds.append((n, h))
        return t_bonds

    @staticmethod
    def find_line(content, atom):
        for line in content:
            if line[12:16].strip() == atom:
                return line
    
    @staticmethod
    def write_line(line, atomname, coords):
        return "{:12s}{:4s}{:14s}{:8.3f}{:8.3f}{:8.3f}{}".format(line[:12], atomname, line[16:30], *coords, line[54:])

    @staticmethod
    def opt_rotation_matrix(query, reference):
        """
        finds the optimal rotation matrix according to the Kabsch algorithm
        (rewritten from a Jimmy Charnley's RMSD library)
        :param query: (Nx3) np.array, coordinates of the atoms to be aligned
        :param reference: (Nx3) np.array, coordinates of the reference atoms
        :return: (3x3) np.array, optimal rotation matrix
        """
        u, s, v = np.linalg.svd(np.dot(np.transpose(query), reference))
        if np.linalg.det(u) * np.linalg.det(v) < 0:
            s[-1] = -s[-1]
            u[:, -1] = -u[:, -1]
        return np.dot(u, v)
            
    def get_residues(self):
        """
        parses the PDB file to locate unique residues and bind
        the content of the file to residue identifiers
        :return:
        reslist: list, contains all residue identifiers (resname-resnum-chainID)
        residues: dict, contains resID:content pairs, where content is a list of PDB lines
        """
        reslist = []
        residues = {}
        for line in self.content:
            residue = line[22:26].strip()
            chain_id = line[21:22].strip()
            resname = line[17:20].strip()
            code = "{}-{}-{}".format(resname, residue, chain_id)
            if code not in residues.keys():
                residues[code] = []
            residues[code].append(line)
            if code not in reslist:
                reslist.append(code)
        return reslist, residues
    
    def find_anchor_coordinates(self, line_list=None):
        """
        finds coordinates of the anchor atoms (is it actually the only
        inherited fn used by the BB class?)
        :param line_list: list of lines in which anchors should be found,
        default is the whole PDB content
        :return: (3x3) np.array, coordinates of the anchor atoms
        """
        if not line_list:
            line_list = self.content
        coords = {}
        try:
            for line in line_list:
                name = line[12:16].strip()
                if name in self.anchor_atoms:
                    coords[name] = np.array([float(line[30+8*x:38+8*x]) for x in range(3)])
            return np.vstack([coords[x] for x in self.anchor_atoms])
        except KeyError:
            for line in line_list:
                name = line[12:16].strip()
                if name in self.alt_anchors:
                    coords[name] = np.array([float(line[30+8*x:38+8*x]) for x in range(3)])
            return np.vstack([coords[x] for x in self.alt_anchors])
    
    def exchange_backbone(self, dna_resname, pna_backbone):
        """
        removes the original backbone atoms of DNA and inserts
        PNA backbone atoms instead
        :param dna_resname: name of the DNA residue to be substituted
        :param pna_backbone: a PNA class instance
        :return: None
        """
        base_atoms = []
        for linenum, line in enumerate(self.residues[dna_resname]):
            name = line[12:16].strip()
            if "'" not in name and "P" not in name:
                base_atoms.append(linenum)
        self.residues[dna_resname] = pna_backbone.set_name(dna_resname) + \
            [self.residues[dna_resname][x] for x in base_atoms]
    
    @staticmethod
    def multiply_atoms(three_atoms):
        """
        optional helper fn to use the original Kabsch algorithm
        with 6 atoms instead of 3 just to avoid instabilities
        :param three_atoms: (3x3) np.array, any coordinates
        :return: (6x3) np.array, original coordinates + midpoints
        """
        more_atoms = np.zeros((6, 3))
        more_atoms[:3, :] = three_atoms
        more_atoms[3, :] = (three_atoms[0] + three_atoms[1]) / 2
        more_atoms[4, :] = (three_atoms[0] + three_atoms[2]) / 2
        more_atoms[5, :] = (three_atoms[1] + three_atoms[2]) / 2
        return more_atoms
    
    @staticmethod
    def extract_coords(line):
        if line:
            return np.array([float(line[30:38]), float(line[38:46]), float(line[46:54])])

    def set_name(self, name, res=None):
        """
        renames all atoms in the residue (residue name, residue number, chain ID)
        :param name: dash-delimited name info (resname-chain-resnum)
        :param res: residue in which replacement should take place
        :return: the original content with residue renamed
        """
        resname, residue, chain_id = name.split('-')
        if not res:
            content = self.content[:]
        else:
            content = self.residues[res]
            resname = resname.replace('D', 'X')
        for linenum, line in enumerate(content):
            content[linenum] = "{:16s} {:>3s} {:1s}{:>4s}{}".format(line[:16], resname, chain_id, residue, line[26:])
        return content

######################################################


class Backbone(DNA):
    def __init__(self, modif='PNA'):
        """
        initializes the helper class representing the modified backbone
        :param modif: name of the modification to be performed, by default PNA
        """
        self.modifications = {'PNA': PNA()}
        self.entity = self.modifications[modif]
        self.filename = self.entity.filename
        self.bonds = self.entity.bonds
        self.sp2 = self.entity.sp2
        self.content = [line.strip() for line in open(self.filename) if line.startswith("ATOM")]
        self.atoms = [l.split()[2] for l in self.content]
        self.anchor_atoms = self.entity.anchors
        self.anchor_coords = self.find_anchor_coordinates()
        
    def reset_coords(self):
        """
        Re-reads coords from PDB (pnabb.pdb)
        :return: None
        """
        self.content = [line.strip() for line in open(self.filename) if line.startswith("ATOM")]
    
    def moveby(self, vec):
        """
        translates all coordinates by a specified translation vector
        :param vec: (3x1) np.array, translation vector
        :return: None
        """
        for linenum, line in enumerate(self.content):
            modline = "{:30s}{:8.3f}{:8.3f}{:8.3f}{}".format(
                line[:30],
                float(line[30:38]) - vec[0],
                float(line[38:46]) - vec[1],
                float(line[46:54]) - vec[2],
                line[54:])
            self.content[linenum] = modline
        self.anchor_coords = self.find_anchor_coordinates()
    
    def rotby(self, matr):
        """
        rotates all coordinates according to a specified rotation matrix
        :param matr: (3x3) np.array, the rotation matrix
        :return: None
        """
        mean_orig = np.mean(self.anchor_coords, 0)
        for linenum, line in enumerate(self.content):
            orig_coords = self.extract_coords(line)
            orig_coords -= mean_orig
            rot_coords = np.dot(orig_coords, matr)
            rot_coords += mean_orig
            modline = "{:17s}{:8.3f}{:8.3f}{:8.3f}{}".format(
                line[:30],
                *rot_coords,
                line[54:])
            self.content[linenum] = modline
        self.anchor_coords = self.find_anchor_coordinates()
    
    @staticmethod
    def find_plane(anchor_atoms):
        """
        finds coordinates that describe the plane defined by three
        anchor atoms for later use in coordinate scaling (self.scaleby)
        :param anchor_atoms: (3x3) np.array, contains the coordinates of three anchor atoms
        :return:
        y_unit: unit vector along the line connecting anchors 0 and 1
        x_unit: unit vector along the line perpendicular to y_unit and passing through anchor 2
        y_len: lenght of the vector connecting anchors 0 and 1
        x_len: height of the triangle defined by anchors 0, 1 and 2
        frac_along: fraction of y_len at which x_len is projected
        p0: projection of anchor 0 along y_unit
        p1: projection of anchor 1 along y_unit
        p2: projection of anchor 2 along y_unit
        """
        y = (anchor_atoms[1] - anchor_atoms[0])
        y_len = np.linalg.norm(y)
        y_unit = y / y_len
        u = (anchor_atoms[2] - anchor_atoms[0])
        p = y_unit * np.dot(u, y_unit)
        x = u - p
        x_len = np.linalg.norm(x)
        x_unit = x / x_len
        frac_along = np.linalg.norm(p)/np.linalg.norm(y)
        p0, p1, p2 = [np.dot(y_unit, anchor_atoms[i]) for i in range(3)]
        q0, q1, q2 = [np.dot(x_unit, anchor_atoms[i]) for i in range(3)]
        return y_unit, x_unit, y_len, x_len, frac_along, p0, p1, p2, q0, q1, q2
    
    def scaleby(self, anchor_atoms):
        """
        scales the coordinates in the plane defined by the anchor atoms
        so that the three anchors of PNA can be exactly fitted onto
        the three anchors of DNA
        :param anchor_atoms: (3x3) np.array, contains the coordinates of DNA's anchor atoms
        :return: None
        """
        along_bb, perpend_bb, dist_along, dist_perpend, current_fract_along_proj, p0, p1, p2, q0, q1, q2 = \
            self.find_plane(self.anchor_coords)
        ab, pb, len_y, len_x, target_fract_along_proj, *_ = self.find_plane(anchor_atoms)
        scale_along = len_y/dist_along
        scale_perpend = len_x/dist_perpend
        p2prim = p0 + target_fract_along_proj*(p1-p0)
        shift = p2prim - p2
        for linenum, line in enumerate(self.content):
            orig_coords = self.extract_coords(line)
            proj_along = np.dot(along_bb, orig_coords)
            # new_proj_along = proj_along + q*(np.min(((proj_along-p0)/(p2-p0), (p1-proj_along)/(p1-p2))))**2
            proj_perpend = np.dot(perpend_bb, orig_coords)
            new_proj_along = proj_along + shift * ((proj_perpend - q0) / (q2 - q0))
            residual = orig_coords - (proj_along*along_bb) - (proj_perpend*perpend_bb)
            new_coords = scale_along*new_proj_along*along_bb + scale_perpend*proj_perpend*perpend_bb + residual
            modline = "{:17s}{:8.3f}{:8.3f}{:8.3f}{}".format(
                line[:30],
                *new_coords,
                line[54:])
            self.content[linenum] = modline
        self.anchor_coords = self.find_anchor_coordinates()


class PNA:
    def __init__(self):
        self.filename = 'pnabb.pdb'
        self.anchors = ("C'", "N1'", "C8'")
        self.bonds = [(9, 18), (8, 9), (8, 16), (8, 17), (7, 8), (7, 14), (7, 15), (6, 7), (3, 6), (1, 6), (3, 12),
                 (3, 13), (3, 4), (4, 5), (1, 2), (0, 1), (0, 10), (0, 11)]
        self.sp2 = [1, 4, 6, 9]


class Optimizer:
    def __init__(self, dna):
        self.coords, self.atoms, self.anchors, self.heavy, self.bonds, self.movable, self.fixed, self.d0s, self.th0s, \
            self.angles, self.sp2 = None, None, None, None, None, None, None, None, None, None, None
        self.dna = dna
        self.dna_sel_coords = None
        self.neighlist = []
        self.sigma_heavy = 3.2
        self.sigma_hydrogen = 1.8
        self.eps = 25
        self.eshg = self.eps * (0.5*(self.sigma_hydrogen+self.sigma_heavy)) ** 12
        self.eshv = self.eps * self.sigma_heavy ** 12
        self.bondheavy_d0 = 1.45
        self.bondhgen_d0 = 1.1
        self.bond_k = 150000
        self.angsp3_th0 = 109
        self.angsp2_th0 = 120
        self.ang_k = 10
        self.dx = 0.0001
        self.a = 0.3
        self.maxiter = 50
        self.conv_e = 40 * len(list(dna.residues))
        self.calc_numerical = False
        self.sigmas = None
        # TODO think of improper dihs or other trans/cis check for peptide bonds
    
    def update(self):
        """
        Reads all necessary info from the DNA instance
        :return: None
        """
        self.coords = np.vstack([self.dna.extract_coords(l) for l in self.dna.content])
        self.atoms = self.dna.atoms
        self.movable = self.dna.movable
        self.fixed = [n for n in range(len(self.atoms)) if n not in self.movable]
        self.anchors = [n for n in range(len(self.atoms)) if self.atoms[n] in self.dna.anchor_atoms]
        self.heavy = set(self.dna.heavy)
        self.sp2 = set(self.dna.sp2)
        self.bonds = self.dna.topo
        self.angles = self.get_angles()
        self.d0s = np.array([self.bondheavy_d0 - (self.bondheavy_d0-self.bondhgen_d0) *
                             (bpr[0] not in self.heavy or bpr[1] not in self.heavy) for bpr in self.bonds])
        self.th0s = np.array([self.angsp2_th0 - (self.angsp2_th0 - self.angsp3_th0) *
                              (a[2] not in self.sp2) for a in self.angles])
        self.select_dna_atoms()
        # TODO issue: do not delete bb atoms from the non-modded chain
    
    def select_dna_atoms(self):
        """
        Selects backbone atoms for optimization
        :return: None
        """
        dna_instance = self.dna
        dna_sel_atoms = []
        
        def criterion(l):
            bb_atoms = ["C8'", "C7'", "O7'", "C5'", "C'", "O1'", "N4'", "C3'", "C2'", "N1'", "H8'1", "H8'2", "H5'1",
                        "H5'2", "H3'1", "H3'2", "H2'1", "H2'2", "H1'1"]
            if l[12:16].strip() not in bb_atoms:
                return True
            else:
                return False
            
        for line in dna_instance.content:
            if criterion(line):
                dna_sel_atoms.append(dna_instance.extract_coords(line))
        self.dna_sel_coords = np.vstack(dna_sel_atoms)
    
    def update_neighlist(self):
        """
        Called every several opt steps, updates the neighbor list
        over which we calculate repulsive interactions
        :return: None
        """
        self.neighlist = []
        bondset = set(self.bonds)
        for ni, i in enumerate(self.movable):
            for j in self.movable[ni+1:]:
                xi = self.coords[i]
                xj = self.coords[j]
                if np.linalg.norm(xi-xj) < 3.0 and (i, j) not in bondset:  # self.bonds contains ordered tuples
                    self.neighlist.append((i, j))
        self.update_ext_neighlist()
        self.sigmas = np.array([(self.sigma_heavy - 0.5*(self.sigma_heavy-self.sigma_hydrogen) *
                                (npr[0] not in self.heavy or npr[1] not in self.heavy))**12 for npr in self.neighlist])
        
    def update_ext_neighlist(self):
        """
        Same as above, but adds pairs for the calculation of
        repulsive interactions with fixed atoms (mostly bases)
        :return: None
        """
        bondset = set(self.bonds)
        for i in self.movable:
            for j in self.fixed:
                xi = self.coords[i]
                xj = self.coords[j]
                if np.linalg.norm(xi-xj) < 3.0 and (i, j) not in bondset and (j, i) not in bondset:
                    self.neighlist.append((i, j))
    
    def get_angles(self):
        """
        Generates a list of angles based on PNA backbone topology
        (list of bonds)
        :return: list of tuples
        """
        angles = []
        for n1, b1 in enumerate(self.bonds):
            for b2 in self.bonds[n1+1:]:
                if len(set(b1).intersection(set(b2))) > 0:
                    angles.append(self.b2a(b1, b2))
        return angles
    
    def calc_energy(self):
        ae = self.calc_angles_e()
        re = self.calc_repulsives_e()
        be = self.calc_bonds_e()
        return ae + re + be
    
    def calc_repulsives_e(self):
        dists = np.array([np.linalg.norm(self.coords[n[0]]-self.coords[n[1]])**12 for n in self.neighlist])
        return np.sum(self.eps * self.sigmas/dists)

    def calc_bonds_e(self):
        dists = np.array([np.linalg.norm(self.coords[n[0]] - self.coords[n[1]]) for n in self.bonds])
        return np.sum(0.5 * self.bond_k * (dists - self.d0s)**2)
    
    def calc_angles_e(self):
        v1s = np.array([self.coords[n[1]] - self.coords[n[0]] for n in self.angles])
        v2s = np.array([self.coords[n[1]] - self.coords[n[2]] for n in self.angles])
        angles = (180/np.pi) * np.arccos(np.sum(v1s*v2s, 1)/(np.linalg.norm(v1s, axis=1)*np.linalg.norm(v2s, axis=1)))
        return np.sum(0.5 * self.ang_k * (angles - self.th0s)**2)
        # e = 0
        # for i in self.angles:
        #     e += self.calc_angle_e(*i)
        # return e
    
    def calc_angle_e(self, a1, a2, a3):
        v1 = self.coords[a2] - self.coords[a1]
        v2 = self.coords[a2] - self.coords[a3]
        angle = (180/np.pi) * np.arccos(np.dot(v1, v2)/(np.linalg.norm(v1)*np.linalg.norm(v2)))
        if a2 in self.sp2:
            return 0.5 * self.ang_k * (self.angsp2_th0 - angle) ** 2
        else:
            return 0.5 * self.ang_k * (self.angsp3_th0 - angle) ** 2
    
    def calc_grad_num(self):
        grad = np.zeros((len(self.atoms), 3))
        for i in range(len(self.atoms)):
            for j in range(3):
                e0 = self.calc_energy()
                self.coords[i, j] += self.dx
                e1 = self.calc_energy()
                self.coords[i, j] -= self.dx
                grad[i, j] = (e1-e0)/self.dx
        return grad
    
    def calc_grad_an(self):
        grad = np.zeros((len(self.atoms), 3))
        for b in self.bonds:
            grad = self.grad_an_bonds(*b, grad)
        for n in self.neighlist:
            grad = self.grad_an_repulsive(*n, grad)
        for a in self.angles:
            grad = self.grad_an_angles(*a, grad)
        return grad
    
    def grad_an_angles(self, a1, a2, a3, grad):
        if a2 in self.sp2:
            th0 = self.angsp2_th0
        else:
            th0 = self.angsp3_th0
        v1 = self.coords[a1] - self.coords[a2]
        v2 = self.coords[a3] - self.coords[a2]
        v1n = np.linalg.norm(v1)
        v2n = np.linalg.norm(v2)
        dot = np.dot(v1, v2)
        q = dot/(v1n*v2n)
        angle = (180 / np.pi) * np.arccos(q)
        gr1 = self.ang_k * (angle - th0) * (v2*v1n*v2n - dot*v1/v1n) / (np.sqrt(1 - q**2) * v1n**2 * v2n**2)
        gr3 = self.ang_k * (angle - th0) * (v1*v1n*v2n - dot*v2/v2n) / (np.sqrt(1 - q**2) * v1n**2 * v2n**2)
        gr2 = -(gr1+gr3)
        grad[a1, :] -= gr1 * (180 / np.pi)
        grad[a2, :] -= gr2 * (180 / np.pi)
        grad[a3, :] -= gr3 * (180 / np.pi)
        return grad

    def grad_an_bonds(self, b1, b2, grad):
        if b1 in self.heavy and b2 in self.heavy:
            d0 = self.bondheavy_d0
        else:
            d0 = self.bondhgen_d0
        v = self.coords[b2]-self.coords[b1]
        d = np.linalg.norm(v)
        gr = self.bond_k*(d-d0)*v/d
        grad[b1, :] -= gr
        grad[b2, :] += gr
        return grad
    
    def grad_an_repulsive(self, n1, n2, grad):
        if n1 in self.heavy and n2 in self.heavy:
            es = self.eshv
        else:
            es = self.eshg
        v = self.coords[n2]-self.coords[n1]
        d = np.linalg.norm(v)
        gr = 12*(es/(d**13))*v/d
        grad[n1, :] += gr
        grad[n2, :] -= gr
        return grad
    
    def grad_ext_an_repulsive(self, n1, q2, grad):
        n2 = -q2
        es = self.eshv
        v = self.dna_sel_coords[n2] - self.coords[n1]
        d = np.linalg.norm(v)
        gr = 12 * (es / (d ** 13)) * v / d
        grad[n1, :] += gr
        return grad
    
    def steep(self):
        """
        The actual optimization subroutine: checks for the
        convergence of energy, updates gradients and the
        learning rate
        :return:
        """
        a = self.a
        e = np.inf
        i = 0
        while e/1000 > self.conv_e and i < self.maxiter:
            if i % 10 == 0:
                self.update_neighlist()
            e = np.inf
            if self.calc_numerical:
                grad = self.calc_grad_num()
            else:
                grad = self.calc_grad_an()
            for j in self.anchors:
                grad[j] = 0
            grad /= np.linalg.norm(grad)
            grad[self.dna.fixed] = 0
            self.coords -= grad*a
            new_e = self.calc_energy()
            print("geo opt iter {}, current energy {:.3f}, continue until {}".format(i, new_e/1000, self.conv_e))
            while new_e < e:
                e = new_e
                self.coords -= grad * a
                new_e = self.calc_energy()
            i += 1
            
    @staticmethod
    def b2a(b1, b2):
        """
        If two bonds share a common atom, returns an ordered tuple
        of atoms corresponding to a plane angle
        :param b1:
        :param b2:
        :return:
        """
        if b1[0] == b2[0]:
            return b1[1], b1[0], b2[1]
        elif b1[0] == b2[1]:
            return b1[1], b1[0], b2[0]
        elif b1[1] == b2[0]:
            return b1[0], b1[1], b2[1]
        elif b1[1] == b2[1]:
            return b1[0], b1[1], b2[0]
        raise ValueError("{} and {} contain no matching entries".format(b1, b2))


if __name__ == "__main__":
    if sys.argv[1] in ['-h', '--help']:
        print("\n\nConverts DNA to PNA models. When used with PDB files, deoxyribose\nbackbone is morphed "
              "into a peptide one, and a modified structure \nis saved. When used with Gromacs topology files "
              "(.itp or .top),\nthe script adds optimized torsional parameters taken from the article\nby "
              "Jasinski, Feig and Trylska (JCTC, 2018) to regular topology.\n\nFirst parameter is the input file; "
              "if desired, only one chain of the \nPDB file can be morphed if chain name is passed as second argument, "
              "e.g.\n\npython dna2pna.py mystruct.pdb A\n\n")
    elif sys.argv[1].endswith('pdb'):
        chain = sys.argv[2] if len(sys.argv) > 2 else None
        pdbfile = sys.argv[1]
        DNA(pdbfile, chain)
    elif sys.argv[1].endswith('itp') or sys.argv[1].endswith('top'):
        Top(sys.argv[1])
    else:
        raise ValueError("Acceptable file extensions are: .pdb (for structure editing);"
                         ".itp, .top (for topology editing)")
