import sys

class Top:
    def __init__(self, filename_top, ff='amber'):
        self.fname = filename_top
        self.contents = open(self.fname).readlines()
        self.sections = {'h': 'header', 'm': 'moleculetype', 't': 'atoms', 'b': 'bonds', 'p': 'pairs',
                         'a': 'angles', 'd': 'dihedrals', 'i': 'dihedrals', 'f': 'footer'}
        self.dict = {section: self.get_section(section) for section in self.sections.keys()}
        self.res = self.find_pna()  # list of residue numbers
        self.bb = Backbone(ff)
        # TODO make work w/CHARMM?
        self.params = {'b': 1, 'p': 1, 'a': 1, 'd': 9, 'i': 4}
        self.process_top()
        self.save_mod()

    def modify_param(self, ptype):
        section = self.dict[ptype]
        mods = self.bb.get_mod_param(ptype)
        for r in self.res:
            for m in mods.keys():
                ll = self.find_line(ptype, m, r)
                if ll is not None:
                    if ptype in 'd' and len(mods[m]) > 3:  # separate handling of multiple dihedrals
                        assert len(mods[m]) % 3 == 0
                        base = section.pop(ll).rstrip()
                        for q in range(int(len(mods[m])/3)):
                            text = base + "".join("{:>10}".format(a) for a in mods[m][3*q:3*(q+1)]) + '\n'
                            section.insert(ll+q, text)
                    else:
                        section[ll] = section[ll].rstrip() + "".join("{:>10}".format(a) for a in mods[m]) + '\n'
        self.dict[ptype] = section
    
    def find_pna(self):
        section = self.dict['t']
        res = set()
        for l in section:
            lspl = l.split()
            if len(lspl) > 3 and lspl[3].startswith('X') and len(lspl[3]) > 1 and lspl[3][1] in 'TGCAU':
                res.add(int(lspl[2]))
        return list(res)

    def find_line(self, ptype, atoms, res):
        names, nums = self.get_nums(res)
        q = len(atoms)
        section = self.dict[ptype]
        for l in range(len(section)):
            if not section[l][0] in "[;" and all(int(section[l].split()[a]) in names.keys() for a in range(q)):
                if (all([names[int(section[l].split()[a])] == atoms[a] for a in range(q)])
                        or all([names[int(section[l].split()[a])] == atoms[q - a - 1] for a in range(q)])):
                    return l
        # we allow missing dihedrals on terminal residues:
        if not ((res == self.res[0] and "C'-" in atoms) or (res == self.res[-1] and "N1'+" in atoms)):
            print('atoms {} not found'.format(tuple(nums[a] for a in atoms)))
        else:
            print('dihedral {} ignored, assuming {} is a terminal residue'.format(tuple(a for a in atoms), res))

    def get_bos(self, ptype):  # finds line num that starts respective section
        lines = [i for i in range(len(self.contents)) if len(self.contents[i].split()) > 2
                 and self.contents[i].split()[1] == self.sections[ptype]]
        if len(lines) == 0:
            raise RuntimeError("Section {} not found in the input topology file".format(self.sections[ptype]))
        if ptype in 'mtbpad':
            return lines[0]
        elif ptype == 'i':
            return lines[1]

    def get_section(self, ptype):
        order = {'m': 't', 't': 'b', 'b': 'p', 'p': 'a', 'a': 'd', 'd': 'i'}
        if ptype in 'mtbpad':
            return [l for l in self.contents[self.get_bos(ptype):self.get_bos(order[ptype])] if not l.isspace()]
        elif ptype == 'i':
            return [line for line in self.contents[self.get_bos(ptype):] if line.startswith('[ dih')
                    or line.startswith(';  ai') or (len(line.split()) > 4 and line.split()[4] == '4')]
        elif ptype == 'h':
            return self.contents[:self.get_bos('m')]
        elif ptype == 'f':
            term_line = self.contents.index(self.get_section('i')[-1]) + 1
            return self.contents[term_line:]

    def save_mod(self):
        path = self.fname.split('/')
        path[-1] = 'opt' + '-' + path[-1]
        outname = '/'.join(path)
        with open(outname, 'w') as outfile:
            for i in 'hmtbpadif':
                for line in self.dict[i]:
                    outfile.write(line)
                outfile.write('\n')

    def get_nums(self, res):  # one call per residue
        atoms = ("N4'", "N1'", "O1'", "C'", "C2'", "C3'", "C5'", "C7'", "C8'", "O7'")
        names = {}
        nums = {}
        for i in atoms:
            cont = [int(line.split()[0]) for line in self.dict['t'] if len(line.split()) > 7
                    and line.split()[3][0] == 'X' and line.split()[4] == i and int(line.split()[2]) == res]
            if len(cont) > 0:
                names[cont[0]] = i
                nums[i] = cont[0]
        cont_prev = [int(line.split()[0]) for line in self.dict['t'] if len(line.split()) > 7
                     and line.split()[3][0] == 'X' and line.split()[4] == "C'" and int(line.split()[2]) == res-1]
        if len(cont_prev) > 0:
            names[cont_prev[0]] = "C'-"
            nums["C'-"] = cont_prev[0]
        cont_next = [int(line.split()[0]) for line in self.dict['t'] if len(line.split()) > 7
                     and line.split()[3][0] == 'X' and line.split()[4] == "N1'" and int(line.split()[2]) == res+1]
        if len(cont_next) > 0:
            names[cont_next[0]] = "N1'+"
            nums["N1'+"] = cont_next[0]
        return names, nums

    def process_top(self):
        for ptype in 'd':
            self.modify_param(ptype)


class Backbone:
    def __init__(self, ff):
        if ff == 'amber':
            self.mod = {'d':
                            {("C'-", "N1'", "C2'", "C3'"): [0.0, 0.29, 2, 0.0, 0.12, 4, 0.0, 0.10, 5],
                             ("N1'", "C2'", "C3'", "N4'"): [180.0, 1.54, 1, 0.0, 0.46, 3, 0.0, 0.18, 4, 0.0, 0.21, 5],
                             ("C2'", "C3'", "N4'", "C5'"): [0.0, 0.61, 3, 0.0, 0.71, 4, 0.0, 0.16, 5],
                             ("C3'", "N4'", "C5'", "C'"): [0.0, 0.24, 1, 0.0, 0.70, 3, 0.0, 0.42, 4, 0.0, 0.05, 5],
                             ("N4'", "C5'", "C'", "N1'+"): [0.0, 0.24, 3, 0.0, 0.12, 5]}}
        elif ff == 'charmm':
            self.mod = {'d':
                            {("C'-", "N1'", "C2'", "C3'"): [0.0, 0.29, 2, 0.0, 0.12, 4, 0.0, 0.10, 5],
                             ("N1'", "C2'", "C3'", "N4'"): [180.0, 1.54, 1, 0.0, 0.46, 3, 0.0, 0.18, 4, 0.0, 0.21, 5],
                             ("C2'", "C3'", "N4'", "C5'"): [0.0, 0.61, 3, 0.0, 0.71, 4, 0.0, 0.16, 5],
                             ("C3'", "N4'", "C5'", "C'"): [0.0, 0.24, 1, 0.0, 0.70, 3, 0.0, 0.42, 4, 0.0, 0.05, 5],
                             ("N4'", "C5'", "C'", "N1'+"): [0.0, 0.24, 3, 0.0, 0.12, 5]}}
    
    def get_mod_param(self, ptype):
        return self.conv_to_kJmol(self.mod[ptype])
    
    def conv_to_kJmol(self, param_dict):
        converted = {}
        multipliers = (1, 4.184, 1)
        for param in param_dict.keys():
            par_values = param_dict[param]
            converted[param] = [round(par_values[i] * multipliers[i%3], 5) for i in range(len(par_values))]
        return converted


if __name__ == "__main__":
    c = Top(sys.argv[1])
